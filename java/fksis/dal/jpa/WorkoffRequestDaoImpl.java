package fksis.dal.jpa;

import fksis.dal.interfaces.WorkoffRequestDao;
import fksis.domain.entities.worksheet.WorkoffRequest;
import fksis.domain.entities.worksheet.WorkoffRequest_;
import fksis.domain.enums.worksheet.RequestStatus;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * @author Sergey Alexetovich
 */
@Repository
public class WorkoffRequestDaoImpl extends JpaDao<WorkoffRequest, Long> implements WorkoffRequestDao
{
  public List<WorkoffRequest> getSubmittedRequests()
  {
    CriteriaBuilder builder = em.getCriteriaBuilder();
    CriteriaQuery<WorkoffRequest> cq = builder.createQuery(WorkoffRequest.class);
    Root<WorkoffRequest> root = cq.from(WorkoffRequest.class);
    cq.select(root);
    cq.where(builder.equal(root.get(WorkoffRequest_.status), RequestStatus.REQUESTED));
    return em.createQuery(cq).getResultList();
  }
}
