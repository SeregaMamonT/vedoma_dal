package fksis.dal.jpa;

import fksis.dal.interfaces.EripTransactionDao;
import fksis.domain.entities.worksheet.EripTransaction;
import fksis.domain.entities.worksheet.EripTransaction_;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * @author Sergey Alexetovich
 */
public class EripTransactionDaoImpl extends JpaDao<EripTransaction, Long> implements EripTransactionDao
{
  @Override
  public EripTransaction getTransactionByNumber(long transactionNumber)
  {
    CriteriaBuilder builder = em.getCriteriaBuilder();
    CriteriaQuery<EripTransaction> cq = builder.createQuery(EripTransaction.class);
    Root<EripTransaction> root = cq.from(EripTransaction.class);
    cq.select(root);
    cq.where(builder.equal(root.get(EripTransaction_.transactionNumber), transactionNumber));
    return em.createQuery(cq).getSingleResult();
  }
}
