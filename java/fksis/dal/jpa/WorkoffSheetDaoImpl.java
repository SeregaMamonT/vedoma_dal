package fksis.dal.jpa;

import fksis.dal.interfaces.WorkoffSheetDao;
import fksis.domain.entities.worksheet.WorkoffSheet;
import fksis.domain.entities.worksheet.WorkoffSheet_;
import fksis.domain.enums.worksheet.SheetStatus;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * @author Sergey Alexetovich
 */
@Repository
public class WorkoffSheetDaoImpl extends JpaDao<WorkoffSheet, Long> implements WorkoffSheetDao
{

  public List<WorkoffSheet> getCreatedSheets()
  {
    CriteriaBuilder builder = em.getCriteriaBuilder();
    CriteriaQuery<WorkoffSheet> cq = builder.createQuery(WorkoffSheet.class);
    Root<WorkoffSheet> root = cq.from(WorkoffSheet.class);
    cq.select(root);
    cq.where(builder.equal(root.get(WorkoffSheet_.status), SheetStatus.NEW));
    return em.createQuery(cq).getResultList();
  }

  public WorkoffSheet getSheetByRequestId(long requestId)
  {
    CriteriaBuilder builder = em.getCriteriaBuilder();
    CriteriaQuery<WorkoffSheet> cq = builder.createQuery(WorkoffSheet.class);
    Root<WorkoffSheet> root = cq.from(WorkoffSheet.class);
    cq.select(root);
    cq.where(builder.equal(root.get(WorkoffSheet_.request), requestId));
    return em.createQuery(cq).getSingleResult();
  }
}
