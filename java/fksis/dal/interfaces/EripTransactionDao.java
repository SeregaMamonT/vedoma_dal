package fksis.dal.interfaces;

import fksis.domain.entities.worksheet.EripTransaction;

/**
 * @author Sergey Alexetovich
 */
public interface EripTransactionDao extends GenericDao<EripTransaction, Long>
{
  public EripTransaction getTransactionByNumber(long transactionNumber);
}
