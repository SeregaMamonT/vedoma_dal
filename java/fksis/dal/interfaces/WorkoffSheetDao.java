package fksis.dal.interfaces;

import fksis.domain.entities.worksheet.WorkoffSheet;

import java.util.List;

/**
 * @author Sergey Alexetovich
 */
public interface WorkoffSheetDao extends GenericDao<WorkoffSheet, Long>
{
  public List<WorkoffSheet> getCreatedSheets();

  public WorkoffSheet getSheetByRequestId(long requestId);
}
