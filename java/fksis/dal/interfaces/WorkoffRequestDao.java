package fksis.dal.interfaces;

import fksis.domain.entities.worksheet.WorkoffRequest;

import java.util.List;

/**
 * @author Sergey Alexetovich
 */

public interface WorkoffRequestDao extends GenericDao<WorkoffRequest, Long>
{
  public List<WorkoffRequest> getSubmittedRequests();
}
