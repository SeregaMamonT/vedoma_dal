package fksis.domain.enums.worksheet;

public enum RequestStatus {
  REQUESTED,
  TO_BE_PAID,
  PAID,
  REJECTED
}
