package fksis.domain.entities.worksheet;

import javax.persistence.metamodel.SingularAttribute;

/**
 * @author Sergey Alexetovich
 */
public abstract class EripTransaction_
{
  public static volatile SingularAttribute<EripTransaction, Long> transactionNumber;
  public static volatile SingularAttribute<EripTransaction, Double> transactionAmount;
  public static volatile SingularAttribute<EripTransaction, Double> availableAmount;

  public EripTransaction_()
  {
  }
}
