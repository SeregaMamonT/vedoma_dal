package fksis.domain.entities.worksheet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author Sergey Alexetovich
 */
@Entity
@Table(name = "ERIP_TRANSACTION", schema = "MAIN")
public class EripTransaction implements Serializable
{
  private long transactionNumber;
  private double transactionAmount;
  private double availableAmount;

  @Column(name = "TRANSACTION_NUMBER")
  public long getTransactionNumber()
  {
    return transactionNumber;
  }

  public void setTransactionNumber(long transactionNumber)
  {
    this.transactionNumber = transactionNumber;
  }

  @Column(name = "TRANSACTION_AMOUNT")
  public double getTransactionAmount()
  {
    return transactionAmount;
  }

  public void setTransactionAmount(double transactionAmount)
  {
    this.transactionAmount = transactionAmount;
  }

  @Column(name = "AVAILABLE_AMOUNT")
  public double getAvailableAmount()
  {
    return availableAmount;
  }

  public void setAvailableAmount(double availableAmount)
  {
    this.availableAmount = availableAmount;
  }
}
