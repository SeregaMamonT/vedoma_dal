package fksis.domain.entities.worksheet;

import fksis.domain.enums.worksheet.SheetStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Sergey Alexetovich
 */
@Entity
@Table(name = "WORKOFF_SHEET", schema = "MAIN")
public class WorkoffSheet implements Serializable
{
  private long sheetId;
  private WorkoffRequest request;
  private Date issueDate;
  private Date endDate;
  private int hoursCount;
  private SheetStatus status;


  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "SHEET_ID")
  public long getSheetId()
  {
    return sheetId;
  }

  public void setSheetId(long sheetId)
  {
    this.sheetId = sheetId;
  }

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "REQUEST_ID")
  public WorkoffRequest getRequest()
  {
    return request;
  }

  public void setRequest(WorkoffRequest request)
  {
    this.request = request;
  }

  @Column(name = "ISSUE_DATE")
  public Date getIssueDate()
  {
    return issueDate;
  }

  public void setIssueDate(Date issueDate)
  {
    this.issueDate = issueDate;
  }

  @Column(name = "END_DATE")
  public Date getEndDate()
  {
    return endDate;
  }

  public void setEndDate(Date endDate)
  {
    this.endDate = endDate;
  }

  @Column(name = "HOURS")
  public int getHoursCount()
  {
    return hoursCount;
  }

  public void setHoursCount(int hoursCount)
  {
    this.hoursCount = hoursCount;
  }

  @Column(name = "STATUS")
  public SheetStatus getStatus()
  {
    return status;
  }

  public void setStatus(SheetStatus status)
  {
    this.status = status;
  }
}
