package fksis.domain.entities.worksheet;

import fksis.domain.enums.worksheet.SheetStatus;

import javax.persistence.metamodel.SingularAttribute;
import java.util.Date;

/**
 * @author Sergey Alexetovich
 */
public abstract class WorkoffSheet_
{
  public WorkoffSheet_()
  {
  }

  public static volatile SingularAttribute<WorkoffSheet, Long> sheetId;
  public static volatile SingularAttribute<WorkoffSheet, WorkoffRequest> request;
  public static volatile SingularAttribute<WorkoffSheet, Date> issueDate;
  public static volatile SingularAttribute<WorkoffSheet, Date> endDate;
  public static volatile SingularAttribute<WorkoffSheet, Integer> hoursCount;
  public static volatile SingularAttribute<WorkoffSheet, SheetStatus> status;
  public static volatile SingularAttribute<WorkoffSheet, Long> sheetNumber;
}
