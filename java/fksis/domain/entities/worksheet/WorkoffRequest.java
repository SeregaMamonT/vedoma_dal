package fksis.domain.entities.worksheet;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import fksis.domain.entities.*;

import fksis.domain.enums.worksheet.RequestStatus;
import fksis.domain.enums.worksheet.StudyEvent;

@Entity
@Table(name = "WORKOFF_REQUEST", schema = "MAIN")
public class WorkoffRequest implements Serializable
{
  private static final long serialVersionUID = 1L;

  private long requestId;
  private Student student;
  private StudyEvent eventType;
  private Date absenceDate;
  private boolean isReasonable;
  private boolean isIllness;
  private String reason;
  private String subjectName;
  private Employee lecturer;
  private Date workoffDate;
  private RequestStatus status;
  private double paymentAmount;
  private long transactionNumber;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "REQUEST_ID")
  public long getRequestId()
  {
    return requestId;
  }

  public void setRequestId(long requestId)
  {
    this.requestId = requestId;
  }

  @OneToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "STUDENT_ID")
  public Student getStudent()
  {
    return student;
  }

  public void setStudent(Student student)
  {
    this.student = student;
  }

  @Column(name = "EVENT_TYPE")
  public StudyEvent getEventType()
  {
    return eventType;
  }

  public void setEventType(StudyEvent eventType)
  {
    this.eventType = eventType;
  }

  @Column(name = "ABSENCE_DATE")
  public Date getAbsenceDate()
  {
    return absenceDate;
  }

  public void setAbsenceDate(Date absenceDate)
  {
    this.absenceDate = absenceDate;
  }

  @Column(name = "IS_REASONABLE")
  public boolean isReasonable()
  {
    return isReasonable;
  }

  public void setReasonable(boolean isReasonable)
  {
    this.isReasonable = isReasonable;
  }

  @Column(name = "IS_ILLNESS")
  public boolean isIllness()
  {
    return isIllness;
  }

  public void setIllness(boolean isIllness)
  {
    this.isIllness = isIllness;
  }

  @Column(name = "REASON")
  public String getReason()
  {
    return reason;
  }

  public void setReason(String reason)
  {
    this.reason = reason;
  }

  @Column(name = "SUBJECT_NAME")
  public String getSubjectName()
  {
    return subjectName;
  }

  public void setSubjectName(String subjectName)
  {
    this.subjectName = subjectName;
  }

  @OneToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "LECTURER_ID")
  public Employee getLecturer()
  {
    return lecturer;
  }

  public void setLecturer(Employee lecturer)
  {
    this.lecturer = lecturer;
  }

  @Column(name = "WORKOFF_DATE")
  public Date getWorkoffDate()
  {
    return workoffDate;
  }

  public void setWorkoffDate(Date workoffDate)
  {
    this.workoffDate = workoffDate;
  }

  @Column(name = "STATUS")
  public RequestStatus getStatus()
  {
    return status;
  }

  public void setStatus(RequestStatus status)
  {
    this.status = status;
  }

  @Column(name = "PAYMENT_AMOUNT")
  public double getPaymentAmount()
  {
    return paymentAmount;
  }

  public void setPaymentAmount(double paymentAmount)
  {
    this.paymentAmount = paymentAmount;
  }

  @Column(name = "TRANSACTION_NUMBER")
  public long getTransactionNumber()
  {
    return transactionNumber;
  }

  public void setTransactionNumber(long transactionNumber)
  {
    this.transactionNumber = transactionNumber;
  }
}
