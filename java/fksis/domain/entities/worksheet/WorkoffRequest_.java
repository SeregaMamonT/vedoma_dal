package fksis.domain.entities.worksheet;

import fksis.domain.entities.Employee;
import fksis.domain.entities.Student;
import fksis.domain.enums.worksheet.RequestStatus;
import fksis.domain.enums.worksheet.StudyEvent;

import javax.persistence.metamodel.SingularAttribute;
import java.util.Date;

/**
 * @author Sergey Alexetovich
 */
public abstract class WorkoffRequest_
{

  public WorkoffRequest_()
  {
  }

  public static volatile SingularAttribute<WorkoffRequest, Long> requestId;
  public static volatile SingularAttribute<WorkoffRequest, Student> student;
  public static volatile SingularAttribute<WorkoffRequest, StudyEvent> eventType;
  public static volatile SingularAttribute<WorkoffRequest, Date> absenceDate;
  public static volatile SingularAttribute<WorkoffRequest, Boolean> isReasonable;
  public static volatile SingularAttribute<WorkoffRequest, Boolean> isIllness;
  public static volatile SingularAttribute<WorkoffRequest, String> reason;
  public static volatile SingularAttribute<WorkoffRequest, String> subjectName;
  public static volatile SingularAttribute<WorkoffRequest, Employee> lecturer;
  public static volatile SingularAttribute<WorkoffRequest, Date> workoffDate;
  public static volatile SingularAttribute<WorkoffRequest, RequestStatus> status;
  public static volatile SingularAttribute<WorkoffRequest, Double> paymentAmount;
  public static volatile SingularAttribute<WorkoffRequest, Long> transactionNumber;
}
